using System.CommandLine;
using System.Security;
using System.Net;
using System.Text;
using System;

namespace nethat 
{
    public enum TypeOfProxy 
    {
        STATIC,
        PAC,
        DIRECT
    }
    public class ApplicationParams
    {
        private static ApplicationParams Instance = null;
        public static ApplicationParams GetInstance(string[] args = null) {
                if ( ApplicationParams.Instance == null) {
                    if (args != null)
                    {
                        ApplicationParams.Instance = new ApplicationParams(args);
                    }
                    else
                    {
                        throw new ArgumentException("Application needs to be initialized");
                    }
                }
                return Instance;
        }
       private ApplicationParams(string[] args)
        {
           
            root = new RootCommand();
            var urlOption = new Option<Uri>("--url", "Url to visit");
            urlOption.IsRequired = true;
            root.Add(urlOption);

            var showContentOption = new Option<bool>("--show-content", "Show the content received");
            showContentOption.SetDefaultValue(false);
             root.Add(showContentOption);
            
            var proxyUrlOption = new Option<Uri>("--proxy-url", "Proxy URL to access");
            proxyUrlOption.SetDefaultValue("");
             root.Add(proxyUrlOption);
            
            var proxyTypeOption = new Option<TypeOfProxy>("--proxy-type", "Type of Proxy can be STATIC or PAC. Do not use DIRECT.");
            proxyTypeOption.SetDefaultValue(TypeOfProxy.DIRECT);
            root.Add(proxyTypeOption);
            
            var proxyUserName = new Option<string>("--proxy-user", "Proxy Username");
            proxyUserName.SetDefaultValue("");
            root.Add(proxyUserName);
            
            var proxyPassword = new Option<String>("--proxy-password", "Proxy Password");
            proxyPassword.SetDefaultValue("");
            root.Add(proxyPassword);
            
            var useSystemProxy = new Option<bool>("--system-proxy", "Use the .NET default WebProxy implementation and find proxy from System");
            useSystemProxy.SetDefaultValue(false);
            root.Add(useSystemProxy);

            var enableHttpClientVerboseLog = new Option<bool>("--enable-http-log", "Enable verbose http logging");
            enableHttpClientVerboseLog.SetDefaultValue(false);
            root.Add(enableHttpClientVerboseLog);
            
            root.SetHandler( SetArgumentValues, urlOption, showContentOption, proxyUrlOption, proxyTypeOption,proxyUserName, proxyPassword, useSystemProxy, enableHttpClientVerboseLog);
            
            root.Invoke(args);
        }

        
        public void SetArgumentValues(Uri url,
                                     bool showContent,
                                     Uri proxyUrl,
                                     TypeOfProxy pType,
                                     string proxyUserName,
                                     String  proxyPassword,
                                     bool useSystemProxy,
                                     bool enableHttpClientVerboseLog)
        {
            Url = url;
            if (Url == null || !Url.IsAbsoluteUri)
            {
                throw new ArgumentException($"Invalid URL ${Url.ToString()}");
            }
            
            if (useSystemProxy) 
            {
                WebProxy = WebRequest.GetSystemWebProxy();
            }
            else
            {
                SecureString securePassword = null;
                if(!String.IsNullOrEmpty(proxyUserName))
                {
                    if(!String.IsNullOrEmpty(proxyPassword))
                    {
                        securePassword = new SecureString();
                        foreach(var ch in proxyPassword){
                         securePassword.AppendChar(ch);
                        }
                    }
                    else 
                    {
                        throw new ArgumentException("Proxy password should be specified if Proxy User Name is provided");
                    }
                    
                    ICredentials creds = new NetworkCredential(proxyUserName, securePassword);
                    WebProxy = new WebProxyImpl(proxyUrl, creds);
                }
            EnableHttpClientVerboseLogging = enableHttpClientVerboseLog;
            ShowContent = showContent;
            }
        }

        public override string ToString() {
            var sb = new StringBuilder();
            sb.Append($"Url             = {Url.ToString()}\n");
            sb.Append($"ShowContent     = {ShowContent}\n");
            if (WebProxy != null)
            {
                sb.Append($"ProxyConfig     = ProxyHost:{WebProxy.GetProxy(Url)}\n");
            }
            sb.Append($"TypeOfProxy     = {ProxyType}\n");
            sb.Append($"Enable HTTP Client Logging = {EnableHttpClientVerboseLogging}\n");
            return sb.ToString();
        }

        RootCommand root;
    
        public Uri Url {get; private set;}
        public bool ShowContent {get; private set;}
        public IWebProxy WebProxy {get; private set;}
        public TypeOfProxy ProxyType {get; private set;}
        public bool EnableHttpClientVerboseLogging {get; private set;}
    }
}

