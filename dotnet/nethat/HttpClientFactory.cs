using System.Net.Http;
using System.Net;
using System;

namespace nethat
{
    public class HttpClientFactory
    {

        public static HttpClient CreateClient(Uri url)
        {
           var handler = new SocketsHttpHandler();
           var appParams = ApplicationParams.GetInstance();
           if ( appParams.WebProxy != null)
           {
            handler.UseProxy = true;
            handler.Proxy = appParams.WebProxy;
           }
           else
           {
            handler.UseProxy = false;
           }
           var client = new HttpClient(handler);
           client.BaseAddress = url;
           return client;
        }

    }
}