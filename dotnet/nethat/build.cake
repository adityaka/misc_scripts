var target = Argument("target", "Default");
var nethat = "./nethat.csproj";
var FrameworkNet60 = "net6.0";
var FrameworkNetCoreApp311 = "netcoreapp3.1";




Task("Clean").Does(() => {
    var NetHat60CleanSettings = new DotNetCleanSettings();
    NetHat60CleanSettings.Framework = FrameworkNet60;
    var NetHat311CleanSettings = new DotNetCleanSettings();
    NetHat311CleanSettings.Framework = FrameworkNetCoreApp311;
    DotNetClean(nethat, NetHat60CleanSettings);
    DotNetClean(nethat, NetHat311CleanSettings);
});
Task("Restore").IsDependentOn("Clean").Does(() => {
    DotNetRestore(nethat);
});
Task("Build").IsDependentOn("Restore").Does(() => {
    var NetHat60BuildSettings = new DotNetBuildSettings();
    NetHat60BuildSettings.Framework = FrameworkNet60;
    var NetHat311BuildSettings = new DotNetBuildSettings();
    NetHat311BuildSettings.Framework = FrameworkNetCoreApp311;
    DotNetBuild(nethat, NetHat60BuildSettings);
    DotNetBuild(nethat, NetHat311BuildSettings);
});
Task("Publish").IsDependentOn("Build").Does(() => {
    var NetHat60PublishSettings = new DotNetPublishSettings();
    NetHat60PublishSettings.Framework = FrameworkNet60;
    var NetHat311PublishSettings = new DotNetPublishSettings();
    NetHat311PublishSettings.Framework = FrameworkNetCoreApp311;
    DotNetPublish(nethat, NetHat60PublishSettings);
    DotNetPublish(nethat, NetHat311PublishSettings);
});
Task("Default").IsDependentOn("Publish");

RunTarget(target);