﻿using System;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net;


namespace nethat
{

    class Program
    {
        static Task<HttpResponseMessage> GetResponse(HttpClient client) 
        {
            var resTask = client.GetAsync("/");
            return resTask;
        }

      
        static async Task Main(string[] args)
        {
            
            try
            {
                var appParams = ApplicationParams.GetInstance(args);
                Console.Write(appParams.ToString());
                var client = HttpClientFactory.CreateClient(appParams.Url);
                Task<HttpResponseMessage> resTask = GetResponse(client);
                await resTask;
            
                var response = resTask.Result;
                Console.WriteLine(response.Headers.ToString());
                if (appParams.ShowContent)
                {
                    var content = response.Content.ReadAsStringAsync();
                    Console.WriteLine(content.Result);
                }
            }
            catch (Exception e) 
            {
                Console.WriteLine($"Error Occurred :{e.Message}");
                Console.WriteLine($"StackTrace:\n {e.StackTrace}");
            }
                
            
        }
    }
}
