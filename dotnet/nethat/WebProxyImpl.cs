using System;
using System.Net;
using System.Collections.Generic;
using System.Text;

namespace nethat
{
    class WebProxyImpl : IWebProxy
    {

        public WebProxyImpl(Uri proxyUrl, ICredentials proxyCreds = null, Uri[] bypassList = null)
        {
            ProxyUrl = proxyUrl;
            Credentials = proxyCreds;
            if (bypassList != null) 
            {
                BypassList =  new List<Uri>(bypassList);
            }
        }

        public Uri GetProxy(Uri destination)
        {
            return ProxyUrl;
        }

        public bool IsBypassed(Uri host)
        {
            return host.IsLoopback;
        }

        public Uri ProxyUrl {get; set;}
        public ICredentials Credentials { get; set; }
        public List<Uri> BypassList {get; set;}

    }
}